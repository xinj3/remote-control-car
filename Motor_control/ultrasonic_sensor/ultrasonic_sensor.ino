const int trigPin1 = 11;
const int echoPin1 = 12;
const int trigPin2 = 5;
const int echoPin2 = 6;
const int BuzzerPin = 3;

long duration1, duration2;
long distance1, distance2;

void setup() {
  // put your setup code here, to run once:
  
  pinMode(trigPin1,OUTPUT);
  pinMode(echoPin1,INPUT);
  pinMode(trigPin2,OUTPUT);
  pinMode(echoPin2,INPUT);
  pinMode(BuzzerPin, OUTPUT);
  Serial.begin(9600);
  
}

void loop() {
  
  digitalWrite(trigPin1, LOW);
  delayMicroseconds(2);

  digitalWrite(trigPin1, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin1, LOW);

  duration1 = pulseIn(echoPin1, HIGH);
  distance1 = duration1 * 0.034/2;

  digitalWrite(trigPin2, LOW);
  delayMicroseconds(2);

  digitalWrite(trigPin2, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin2, LOW);

  
  
  duration2 = pulseIn(echoPin2, HIGH);
  distance2 = duration2 * 0.034/2;

  Serial.println(distance1);
  Serial.println(distance2);
  
  
  if ((distance2 < 10) || (distance1 < 10))
  {    
    digitalWrite(BuzzerPin, HIGH);
    delay(250);
    digitalWrite(BuzzerPin, LOW);
  }
}
  // put your main code here, to run repeatedly:
