/*
   left motor: 9, 13
   right motor:8, 10
*/
int input1;
int input = '0'; //input to be used
String input2; //variable for string input

const int trigPin1 = 11;
const int echoPin1 = 12;
const int trigPin2 = 5;
const int echoPin2 = 6;
const int BuzzerPin = 3;

long duration1, duration2;
long distance1, distance2;

const int LED_PET = 4;

void setup() {

  pinMode(trigPin1, OUTPUT);
  pinMode(echoPin1, INPUT);
  pinMode(trigPin2, OUTPUT);
  pinMode(echoPin2, INPUT);
  pinMode(BuzzerPin, OUTPUT);
  pinMode(LED_PET, OUTPUT);

  pinMode(13, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(10, OUTPUT);

  Serial.begin(9600);


}

void loop() {

  digitalWrite(trigPin1, LOW);
  delayMicroseconds(2);

  digitalWrite(trigPin1, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin1, LOW);

  duration1 = pulseIn(echoPin1, HIGH);
  distance1 = duration1 * 0.034 / 2;

  digitalWrite(trigPin2, LOW);
  delayMicroseconds(2);

  digitalWrite(trigPin2, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin2, LOW);

  duration2 = pulseIn(echoPin2, HIGH);
  distance2 = duration2 * 0.034 / 2;

  //Serial.println(distance1);
  //Serial.println(distance2);

  /*
    if ((distance2 < 10) || (distance1 < 10))
    {
    digitalWrite(BuzzerPin, HIGH);

    }
  */

  input1 = Serial.read();
  if (input1 != -1) {
    input = input1;
  }

  Serial.println( input);
  if (input == '5') {
    digitalWrite(LED_PET, HIGH);
    if ( distance1 > 90) {
      digitalWrite(13, HIGH);
      digitalWrite(9, LOW);
      digitalWrite(8, LOW);
      digitalWrite(10, HIGH);
      delay(500);
    }

    else if ( distance1 < 50) {
      digitalWrite(13, LOW);
      digitalWrite(9, HIGH);
      digitalWrite(8, HIGH);
      digitalWrite(10, LOW);
      delay(500);
    }
    if ((distance1 < 90) && (distance1 > 50)) {
      digitalWrite(13, LOW);
      digitalWrite(9, LOW);
      digitalWrite(8, LOW);
      digitalWrite(10, LOW);

    }

  }




  if (input != '5') {
     digitalWrite(LED_PET, LOW);
    // forward
    if ((input == '1') )
    {
      if (distance1 < 20) {
        digitalWrite(13, LOW);
        digitalWrite(9, LOW);
        digitalWrite(8, LOW);
        digitalWrite(10, LOW);
        digitalWrite(BuzzerPin, HIGH);
        delay(500);

      }
      else {
        digitalWrite(13, HIGH);
        digitalWrite(9, LOW);
        digitalWrite(8, LOW);
        digitalWrite(10, HIGH);
        delay(500);
      }
    }

    //back
    else if ((input == '2') )
    {
      if (distance2 < 20) {
        digitalWrite(13, LOW);
        digitalWrite(9, LOW);
        digitalWrite(8, LOW);
        digitalWrite(10, LOW);
        digitalWrite(BuzzerPin, HIGH);
        delay(500);

      }
      else {
        digitalWrite(13, LOW);
        digitalWrite(9, HIGH);
        digitalWrite(8, HIGH);
        digitalWrite(10, LOW);
        delay(500);
      }
    }

    //right
    else if (input == '3')
    {
      digitalWrite(13, HIGH);
      digitalWrite(9, LOW);
      digitalWrite(8, HIGH);
      digitalWrite(10, LOW);
      delay(500);
    }

    //left
    else if (input == '4')
    {
      digitalWrite(13, LOW);
      digitalWrite(9, HIGH);
      digitalWrite(8, LOW);
      digitalWrite(10, HIGH);
      delay(500);
    }


    //default state
    else if ((input == '0') || (input == '6'))
    {
      digitalWrite(13, LOW);
      digitalWrite(9, LOW);
      digitalWrite(8, LOW);
      digitalWrite(10, LOW);
    }


    Serial.println(input);
    //Serial.println(distance1);
    //Serial.println(distance2);

    digitalWrite(BuzzerPin, LOW);
  }
}
